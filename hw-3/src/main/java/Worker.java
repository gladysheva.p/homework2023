import animals.Animal;
import food.Food;
import foodException.WrongFoodException;

public class Worker {
    public void feed(Animal animal, Food food) {
        try {
            animal.eat(food);
            System.out.println(animal.getName() + " ate " + food.getName() + ".");
        } catch (WrongFoodException e) {
            e.printStackTrace();
        }
    }
}



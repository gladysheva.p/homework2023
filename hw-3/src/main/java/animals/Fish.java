package animals;

import aviary.AviarySize;
import interfaces.Swim;

public class Fish extends Carnivorous implements Swim {
    public Fish(String name, String color, int weight, int age) {
        this.name = name;
        this.color = color;
        this.weight = weight;
        this.age = age;
        this.size = AviarySize.SMALL;
    }

    @Override
    public void swim() {
        System.out.println(this.name + " likes to swim!");
    }
}

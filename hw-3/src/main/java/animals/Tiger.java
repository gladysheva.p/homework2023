package animals;

import aviary.AviarySize;
import interfaces.Run;
import interfaces.Voice;

public class Tiger extends Carnivorous implements Run, Voice {
    public Tiger(String name, String color, int weight, int age) {
        this.name = name;
        this.color = color;
        this.weight = weight;
        this.age = age;
        this.size = AviarySize.LARGE;
    }

    @Override
    public void run() {
        System.out.println(this.name + " likes to run!");
    }

    @Override
    public String voice() {
        return "Rrrrr";
    }
}

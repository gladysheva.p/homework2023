package animals;

import food.Food;
import food.Meat;
import foodException.WrongFoodException;

public abstract class Carnivorous extends Animal {
    @Override
    public final void eat(Food food) throws WrongFoodException {
        if (!(food instanceof Meat))
            throw new WrongFoodException();
        System.out.println("This animal eat a meat.");
        this.addSatiety(5);
    }
}


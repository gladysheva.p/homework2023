import animals.*;
import aviary.Aviary;
import aviary.AviarySize;
import food.Grass;
import food.Meat;

public class Zoo {
    public static void main(String[] args) {
        Fish fish1 = new Fish("Boris", "Yellow", 5, 1);
        Fish fish2 = new Fish("Arkasha", "Green", 2, 4);
        Duck duck1 = new Duck("Bob", "Black", 3, 3);
        Duck duck2 = new Duck("Robert", "Gray", 3, 5);
        Rabbit rabbit1 = new Rabbit("Bony", "White", 1, 1);
        Fox fox1 = new Fox("Alisa", "Orange", 9, 5);
        Tiger tiger1 = new Tiger("Vova", "Orange", 20, 6);
        Cow cow1 = new Cow("Mila", "White", 23, 1);

        Grass grass1 = new Grass("dill");
        Meat meat1 = new Meat("steak");

        Worker worker = new Worker();

        Aviary<Herbivores> herbivoreAviary = new Aviary<>(AviarySize.EXTRA_LARGE);
        Aviary<Carnivorous> carnivoreAviary = new Aviary<>(AviarySize.LARGE);

        herbivoreAviary.add(rabbit1);
        herbivoreAviary.add(duck1);

        carnivoreAviary.add(fox1);
        carnivoreAviary.add(tiger1);

        System.out.println("Herbivore aviary:");
        herbivoreAviary.viewAviary();
        System.out.println();
        System.out.println("Carnivore aviary:");
        carnivoreAviary.viewAviary();

        System.out.println();
        System.out.println("Get a link to animal duck1 by its name:");
        System.out.println(herbivoreAviary.getAnimal("Bob"));
        System.out.println();

        worker.feed(fox1, grass1);
        worker.feed(fox1, meat1);

    }
}

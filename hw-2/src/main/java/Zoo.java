import animals.Duck;
import animals.Fish;
import animals.Fox;
import animals.Rabbit;
import food.Grass;
import food.Meat;
import interfaces.Swim;

import java.util.ArrayList;
import java.util.Arrays;

public class Zoo {
    public static void main(String[] args) {
        Fish fish1 = new Fish("Boris", "Yellow", 5, 1);
        Fish fish2 = new Fish("Arkasha", "Green", 2, 4);
        Duck duck1 = new Duck("Bob", "Black", 3, 3);
        Duck duck2 = new Duck("Robert", "Gray", 3, 5);
        Rabbit rabbit1 = new Rabbit("Bony", "White", 1, 1);
        Fox fox1 = new Fox("Alisa", "Orange", 9, 5);

        Worker worker = new Worker();

        Grass grass1 = new Grass("dill");
        Meat meat1 = new Meat("steak");

        worker.feed(fox1, grass1);
        worker.feed(fox1, meat1);
        worker.feed(duck2, grass1);
        worker.feed(fish1, meat1);
        worker.getVoice(fox1);
        worker.getVoice(duck1);

        ArrayList<Swim> pond = new ArrayList<>(Arrays.asList(duck1, duck2, fish1, fish2));
        for (Swim animal : pond)
            animal.swim();
    }

}

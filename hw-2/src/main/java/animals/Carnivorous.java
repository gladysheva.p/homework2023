package animals;

import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal {
    @Override
    public final boolean eat(Food food) {
        if (!(food instanceof Meat)) {
            System.out.println("Carnivorous don't eat grass\n");
            return false;
        }
        System.out.println("Carnivore ate meat");
        this.addSatiety(5);
        return true;
    }
}

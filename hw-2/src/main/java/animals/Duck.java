package animals;

import interfaces.Fly;
import interfaces.Run;
import interfaces.Swim;
import interfaces.Voice;

public class Duck extends Herbivores implements Run, Voice, Swim, Fly {
    public Duck(String name, String color, int weight, int age) {
        this.name = name;
        this.color = color;
        this.weight = weight;
        this.age = age;
    }

    @Override
    public void fly() {
        System.out.println(this.name + " likes to fly!");
    }

    @Override
    public void run() {
        System.out.println(this.name + " likes to run!");
    }

    @Override
    public void swim() {
        System.out.println(this.name + " likes to swim!");
    }

    @Override
    public String voice() {
        return "Quack-quack!";
    }
}
package animals;

import interfaces.Run;
import interfaces.Voice;

public class Cow extends Herbivores implements Run, Voice {
    public Cow(String name, String color, int weight, int age) {
        this.name = name;
        this.color = color;
        this.weight = weight;
        this.age = age;
    }

    @Override
    public void run() {
        System.out.println(this.name + " likes to run!");
    }

    @Override
    public String voice() {
        return "Muuuu";
    }
}
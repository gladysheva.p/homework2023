package animals;

import food.Food;
import food.Grass;

public abstract class Herbivores extends Animal {
    @Override
    public final boolean eat(Food food) {
        if (!(food instanceof Grass)) {
            System.out.println("Herbivores don't eat meat\n");
            return false;
        }
        System.out.println("Herbivore ate grass");
        this.addSatiety(3);
        return true;

    }
}

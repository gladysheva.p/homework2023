package animals;

import food.Food;

public abstract class Animal {
    protected String name;
    protected int age;
    protected int weight;
    protected String color;
    protected int satiety = 10;

    public abstract boolean eat(Food food);

    public String getName() {
        return name;
    }
    public int getSatiety() {
        return satiety;
    }

    public void addSatiety(int s) {
        satiety += s;
    }
}


